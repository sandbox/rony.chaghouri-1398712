jQuery(document).ready(function()
{
    var userRegisterField = jQuery('.field-type-user-register-field input:last');
    if (userRegisterField.val() != null)
    {
        userRegisterField.hide();
        var basepath = Drupal.settings.basePath;
        jQuery.ajax(
        {
           type: "POST",
           url: basepath + "user_register_field/generateUserCreateForm/",
           dataType:'json',
           success: function(json)
           {
            if (json != 0)
            {
                userRegisterField.before(json.output);
                jQuery('#user-register-form #edit-submit').remove();
                jQuery('.node-form #edit-actions #edit-submit').click(function()
                {
                    userRegister(basepath);
                    return false;
                });
            }
           },
          error: function(json)
          {
            alert('Error during user registration');
          }
        });
    }   
});

function userRegister(basepath)
{
    var dataString = jQuery('#user-register-form').serialize();
    jQuery.ajax(
    {  
      type: "POST",  
      url: basepath + 'user/register',
      dataType:'html',
      data: dataString,                      
      async: false,
      success: function(html) 
      {  
          var error = html.replace(/\n/g, ''); 
          error = error.match(/<div class="messages error">.*?(.*?)\s*<\/div>/mi);
          if (error != null && typeof(error[0]) != "undefined")
          {
                if (jQuery('.messages.error').val() != null)
                    jQuery('.messages.error').remove();
                jQuery('#user-register-form').before(error[0]);
          }
          else
          {
              nodeSave(basepath);
          }       
      }
    });
}

function nodeSave(basepath)
{
    var nodeForm = jQuery('.node-form');
    var nodeDataString = nodeForm.serialize();
    jQuery.ajax(
    {  
      type: "POST",  
      url: nodeForm.attr('action'),
      dataType: 'html',
      data: nodeDataString,                      
      async: false,
      success: function(html) 
      {  
          var error = html.replace(/\n/g, ''); 
          error = error.match(/<div class="messages error">.*?(.*?)\s*<\/div>/mi);
          if (error != null && typeof(error[0]) != "undefined")
          {
                if (jQuery('.messages.error').val() != null)
                    jQuery('.messages.error').remove();
                nodeForm.before(error[0]);
          }
          else
          {
              var nodePage = html.replace(/\n/g, ''); 
              nodePage = nodePage.match(/id="node-.*?(.*?)\s*"/mi);
              if (nodePage != null && typeof(nodePage[0]) != "undefined")
              {
                  var nid = nodePage[1];
                  window.location.replace(basepath + 'node/' + nid);
              }
          }       
      }
    });
}